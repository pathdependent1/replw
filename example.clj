;; a simple todo list application

;= <abstract>HELPERS

(defmacro literal-union
  [literals]
  `(s/or ~@(map (fn [x#] `(partial = ~x#)) literals)))

;= SPECS

(s/def ::todo-status
  (literal-union [
                  :todo
                  :in-progress
                  :task-completed]))

(s/def ::todo-item
  (s/tuple string? ::todo-status))

(s/def ::todos
  (s/coll-of ::todo-item
             :kind vector?))

;= <specific>HELPERS

(s/fdef toggle-todo-status
  :args ::todo-item
  :ret ::todo-item)

(defn toggle-todo-status
  [[name status]]
  [
   name
   (case status
     :todo              :in-progress
     :in-progress       :task-completed
     :task-completed    :todo)))

;= MODEL

(s/def ::model-schema
  (s/schema [
             ::todos]))

(replw/model! ::model-schema {::todos #{}})

;= UPDATES

(replw/defupdate new-todo
  [item], [::todos] model
  (replw/expr
   `(update-for-user! (partial conj [~item :todo])
                      :todos))
  #(assoc model :todos %))

(replw/defupdate change-status
  [i], [::todos] model
  (replw/expr
   `(update-for-user! #(update % i toggle-todo-status)
                      :todos))
  #(assoc model :todos %))

(replw/defupdate delete-todo
  [i], [::todos] model
  (replw/expr
   `(update-for-user! #(update % i nil)))
  #(assoc model :todos %))

;= VIEWS

(replw/defview todo-list
  [::todos] model
  (replw/html
   [[:div :ul]
    (map (fn [[i [todo-name todo-status]]]
           [:li {:onclick (replw/make-update change-status i)}
            [:div
             [:b todo-name] " (" (todo-status-text todo-status) ")"
             [:button {:onclick (replw/make-update delete-todo i)}
              "Delete"]]])
         (range)
         (filter (complement nil?)
                 (model ::todos))))))
