# REPL-W

The specification of and a simple implementation for **REPL-W**.

**REPL-W** is a somewhat standardized format for web frontend and how they communicate with backend services. 

Essentially, there is a thin layer, called a *shim*, that sits in front of the backend server and exposes a
*Web REPL* which anyone can interact with over a WebSockets connection. On the frontend, user actions yield
*updates*, which are represented as an expression sent to the *Web REPL* combined with a function that
transforms the result into something that updates the *model*. The *view* is derived purely from the *model*.

Thus, a **REPL-W** system consists of the following:

1. A *shim*, which defines all the ways that the frontend and the backend can interact in the form of
functions and data.
2. A *model*, which represents current application state.
3. Some *views*, which converts a *model* into HTML, it is of the type `Model => HTML`.

The HTML that the *views* return can also contain JavaScript which yields *updates*, they are of the type
`Model => (Expression[A] × (A => Model))`.
